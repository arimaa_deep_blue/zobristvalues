import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 *
 */
public class Main {

	private static final int       n               = 6;
	private static       int       m               = 3;
	private static final int       mMax            = 50;
	private static       int       current_k       = 3;
	private static       long[][]  solutionCount   = { { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 },
	                                         { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 },
	                                         { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 },
	                                         { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 },
	                                         { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 },
	                                         { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 },
	                                         { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 },
	                                         { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 },
	                                         { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 },
	                                         { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 },
	                                         { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 },
	                                         { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 },
	                                         { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 },
	                                         { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 },
	                                         { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 },
	                                         { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 } };
	private static       long[]    values          = new long[mMax];
	static               Set<Long> meetInTheMiddle = new HashSet<>();

	public static void main(String[] args) {
		Logging.setup();
		setup();
		bruteForceMEqualsFour();

		for (int i = 0; i < solutionCount.length; i++) {
			Logging.printLine("m = " + (i + 4) + ", Solution count:\n4: " + solutionCount[i][0] + "\n5: " + solutionCount[i][1]
			                   + "\n6: " + solutionCount[i][2] + "\n7: " + solutionCount[i][3]);

		}
		Logging.close();
	}

	private static void bruteForceMEqualsFour() {
		while(iterate(3)) {
			if (verify(3)) {
				m = 4;
				biggerMRecursion(m);
				m = 3;
			}
		}
	}

	private static void biggerMRecursion(int m) {
		for (long j = values[m - 2] + 1; j < (1L << n); j++) {
			values[m - 1] = j;
			current_k = 3;
			if (verify(m)) {
				current_k = 4;
				if (checkHigherK()) {
					Main.m++;
					biggerMRecursion(Main.m);
					Main.m--;
				}
				current_k = 3;
			}
		}
	}

	private static boolean checkHigherK() {
		boolean successfulFour = false;
		while (current_k <= m) {
			if (verify(m)) {
				if (current_k == m || current_k >= 4) {
					successfulFour = true;
				}
				if (solutionCount[m - 4][current_k - 4] % 10000 == 0) {
					Logging.printLine("Current m: " + m + ", Current k: " + current_k);
					String valuesString = "";
					for (int l = 0; l < m; l++) {
						valuesString += values[l] + " ";
					}
					Logging.printLine(valuesString + "\n");
				}
				solutionCount[m - 4][current_k - 4]++;
			} else {
				break;
			}
			current_k++;
		}
		return successfulFour;
	}

	private static void setup() {
		for (int i = 0; i < m; i++) {
			values[i] = i + 1;
		}
	}

	public static boolean iterate(int m) {
		for (int i = m - 1; i >= 0; i--) {
			if (values[i] < ((1L << n) - m + i)) {
				values[i]++;
				for (int j = i + 1; j < m; j++) {
					values[j] = values[j - 1] + 1;
				}
				return true;
			}
		}
		return false;
	}

	public static boolean verify(int m) {
		meetInTheMiddle.clear();
		hashTableRecursion(0, m, 0, (current_k - 1) / 2);
		return validation(0, m, 0, current_k / 2 + 1);
	}

	private static void hashTableRecursion(long value, int m, int arrayPosition, int depthLeft) {
		for (int i = arrayPosition; i < m - depthLeft + 1; i++) {
			value ^= values[i];
			if (depthLeft == 1) {
				meetInTheMiddle.add(value);
			} else {
				hashTableRecursion(value, m, i + 1, depthLeft - 1);
			}
			value ^= values[i];
		}
	}

	private static boolean validation(long value, int m, int arrayPosition, int depthLeft) {
		for (int i = arrayPosition; i < m - depthLeft + 1; i++) {
			value ^= values[i];
			if (depthLeft == 1) {
				if (meetInTheMiddle.contains(value)) {
					return false;
				}
			} else {
				if (!validation(value, m, i + 1, depthLeft - 1)) {
					return false;
				}
			}
			value ^= values[i];
		}
		return true;
	}
}
